<!DOCTYPE html>
<html lang = "en-US">

<head>
    <meta charset = "UTF-8">    
    <title> Movie Database Test Page </title>
</head>
<body>
<h1> Movie Database Information</h1>

<?php
    // Create connection
    $con = mysqli_connect('10.8.0.1:22', 'melissa', 'melissaArizona', 'finalproject.movies');
    // Check connection
    if (mysqli_connect_errno()) {
        echo "Connection failed: " . mysqli_connect_error();
    }

    $result = mysqli_query($con,"SELECT * FROM film");

    // output data of each row
     while($row = mysqli_fetch_array($result)) {
        echo $row["id"], $row["original_title"], $row["overview"], $row["release_date"], $row["genre"];
        echo "<br />";
    }

    mysqli_close($con);
?>

</body>
</html>