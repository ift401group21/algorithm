import numpy as np
import pandas as pd
from flask import Flask, request, jsonify, render_template
import mysql.connector
app = Flask(__name__)

#get the movie data from themoviedb database/our database
movie_data = mysql.connector.connect(
    host= "10.8.0.1:22",
    user= "homelab",
    password= "longerArizona12!"
)

#merging info we want to base our recommendations off of
ratings = pd.merge(title, movie_like)

# it will automatically standardize the ratings
item_similarity_df = ratings.corr(method='pearson')
item_similarity_df.to_csv('item_similarity_df.csv') 

# make recommendations
# this method will return a similarity score
def get_similar_movies(title,movie_like):
    similar_score = item_similarity_df[title]*(float(movie_like)-2.5)
    similar_movies = similar_score.sort_values(ascending=False)
    return similar_movies
  
def getRecommendations(title,movie_like):
    try:
        similar_movies = pd.DataFrame()
        similar_movies = similar_movies.append(get_similar_movies(title,movie_like),ignore_index=True)
        all_recommend = similar_movies.sum().sort_values(ascending=False)
        m = all_recommend[0:15].to_string()
        m = m.split("\n")
        l=[]
        for i in m:
            i = i.split("  ")
            l.append(i[0])
        return l
        s = ""
        for i in l:
            s+=i+"  "
        return(s)
    except:
        return("Sorry No suggestions available !")