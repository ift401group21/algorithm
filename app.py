from flask import Flask, render_template, redirect, request, url_for
from flask_login import login_user,login_required,logout_user,current_user
from myproject.models import User,Movies,MovieLike
from myproject.forms import LoginForm, RegistrationForm, SearchForm
from myproject import app,db,login_manager
from werkzeug.security import generate_password_hash, check_password_hash

@app.context_processor
def base():
	form = SearchForm()
	return dict(form=form)
   
@app.route('/search', methods=["POST"])
def search():
    form = SearchForm()
    movies= Movies.query
    if form.validate_on_submit():
        movie.searched = form.searched.data
        movies = movies.filter(Movies.title.like('%' + movie.searched + '%'))
        #Return by Stars
        movies = movies.order_by(Movies.stars).all()
        return render_template("search.html", searched=movie.searched, form=form, movies=movies)

@app.route('/profile')
@login_required
def profile():
    user=current_user
    return_movies=db.session.query(Movies.id,Movies.title).filter(User.id==MovieLike.user_id).filter(MovieLike.movies_id==Movies.id).filter(User.id==user.id)
    return render_template('profile.html',user=user,return_movies=return_movies)
    
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
    
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(uname=form.username.data).first()
        if user is not None and user.check_password(form.password.data):
            login_user(user)
            next = request.args.get('next')
            if next == None or not next[0]=='/':
                next = url_for('profile')
            return redirect(next)
    return render_template('login.html', form=form)    

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', form=form)

@app.route('/')
def home():
    #limit may break shit
    top_movies=Movies.query.order_by(Movies.stars.desc()).limit(21)
    return render_template("home.html",top_movies=top_movies)


@app.route('/<int:id>')
def movie(id):
    movies=Movies.query.get(id)
    mlikes= Movies.query.filter_by(id=movies.id).first_or_404()
    return render_template("movie.html",movies=movies,mlikes=mlikes)

@app.route('/<int:id>/<action>')
@login_required
def like_action(id, action):
    movies = Movies.query.filter_by(id=id).first_or_404()
    if action == 'like':
        current_user.like_movie(movies)
        movies.stars+=1
        db.session.commit()
    if action == 'unlike':
        current_user.unlike_movie(movies)
        movies.stars-=1
        db.session.commit()
    return redirect(request.referrer)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)