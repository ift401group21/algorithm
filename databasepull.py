import requests
import pandas
import pymysql
from sqlalchemy import create_engine
import json

conn = pymysql.connect(host='192.168.1.223',
                       port=3306,
                       user='adder', 
                       passwd='arizona12!',  
                       db='finalproject',
                       charset='utf8')

engi = create_engine('mysql+pymysql://adder:arizona12!@192.168.1.223:3306/finalproject', echo=False)

for x in range(244752,990000):
    mr = requests.get('https://api.themoviedb.org/3/movie/'+str(x)+'?api_key=b02ae426945fb8eab4c3318a3244fd0d&language=en-US')
    cr = requests.get('https://api.themoviedb.org/3/movie/'+str(x)+'/credits?api_key=b02ae426945fb8eab4c3318a3244fd0d&language=en-US')
    
    #pr=json.loads(cr.text)
    if cr.status_code==200:
        pr=pandas.json_normalize(cr.json(), 'crew')
        castdf=pandas.json_normalize(cr.json(), 'cast')

        try:
            print(mr.json()['adult'])
        except:
            print(str(x)+" ID doesnt exist")
        else:
            my_dict = pandas.json_normalize(mr.json())

            #load the genres
            v=mr.json()   
            p_genre=''
            for i in v['genres']:
                p_genre+=str(i['name'])+', '
            my_dict['genre']=p_genre

            if 'job' in pr:
                wrdf = pr.loc[(pr['job']=='Writer')]
                my_dict['writers']=str(wrdf[['name']].values.tolist()).replace('[','').replace(']','').replace('\'','')
                #load directors
                drdf = pr.loc[(pr['job']=='Director')]
                my_dict['director']=str(drdf[['name']].values.tolist()).replace('[','').replace(']','').replace('\'','')
                #load producers
                prdf = pr.loc[(pr['job']=='Producer')]
                my_dict['producer']=str(prdf[['name']].values.tolist()).replace('[','').replace(']','').replace('\'','')
            else:
                my_dict['writers']=''
                my_dict['director']=''
                my_dict['producer']=''

            if not castdf.empty:
                cadf = castdf.loc[(castdf['known_for_department']=='Acting')]
                my_dict['actors']=str(cadf[['name']].values.tolist()).replace('[','').replace(']','').replace('\'','')
            else:
                my_dict['actors']=''

            my_dict = my_dict[['id','original_title','overview','release_date','genre','director','writers','actors','producer']]
            my_dict.rename({'original_title': 'title', 'overview': 'description','release_date': 'rel_date'}, axis=1, inplace=True)
            try:
                my_dict.to_sql(name='movies', con=engi, if_exists = 'append', index=False)
                print("uploaded id: " + str(x))
            except:
                print("error on id: " + str(x))

            #print(pr2[['name']].values.flatten().tolist())

    #print(my_dict)

